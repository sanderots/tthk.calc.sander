using Calc.ApplicationServices;
using Calc.Core.Domain;
using System;
using Xunit;

namespace Calc.Test
{
    public class AddOperator
    {
        [Fact]
        public void Should_BeAbleToAddTwoNumbersTogether()
        {
            var op = new Core.Domain.AddOperator();
            var result = op.Exec(1, 2);
            Assert.Equal(3, result);
        }
    }

    public class CalculatorService
    {
        //[Fact]
        //public void Should_BeAbleToEvaluateStringExpression()
        //{
        //    var svc = new ApplicationServices.CalculatorService();

        //    var result = svc.EvalExpression("1*2");

        //    Assert.Equal(2, result);
        //}

        [Fact]
        public void Should_BeAbleToEvaluateTree()
        {
            var rootNode = new BinaryCalcNode(
                new Core.Domain.AddOperator(),
                new ValueCalcNode(1),
                new BinaryCalcNode(
                    new Core.Domain.MultiplyOperator(),
                    new ValueCalcNode(2),
                    new ValueCalcNode(3)
                )
            );

            var result = rootNode.GetValue();

            Assert.Equal(7, result);
        }

        [Fact]
        public void Should_BeAbleToReprasentTreeAsString()
        {
            var rootNode = new BinaryCalcNode(
                new Core.Domain.AddOperator(),
                new ValueCalcNode(1),
                new BinaryCalcNode(
                    new Core.Domain.MultiplyOperator(),
                    new ValueCalcNode(2),
                    new ValueCalcNode(3)
                )
            );

            var result = rootNode.GetString();

            Assert.Equal("1+2*3", result);
        }

    }
}
