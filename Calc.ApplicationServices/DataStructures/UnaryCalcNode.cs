﻿using Calc.Core.Domain;

namespace Calc.ApplicationServices.DataStructures
{
    public class UnaryCalcNode : IUnaryCalcNode
    {
        public UnaryCalcNode(IUnaryOperator @operator, ICalcNode argument)
        {
            Operator = @operator;
            Argument = argument;
        }

        public decimal GetValue()
        {
            return Operator.Exec(Argument.GetValue());
        }

        public IUnaryOperator Operator { get; }
        public ICalcNode Argument { get; }
    }
}
