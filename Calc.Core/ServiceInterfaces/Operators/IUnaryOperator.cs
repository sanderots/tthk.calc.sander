﻿namespace Calc.Core.ServiceInterfaces.Operators
{
    public interface IUnaryOperator : IOperator
    {
        decimal Exec(decimal val);

    }
}
