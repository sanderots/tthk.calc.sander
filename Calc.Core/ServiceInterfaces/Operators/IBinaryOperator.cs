﻿namespace Calc.Core.ServiceInterfaces.Operators
{
    public interface IBinaryOperator : IOperator
    {
        decimal Exec(decimal left, decimal right);
    }
}
